#tool nuget:?package=NUnit.ConsoleRunner&version=3.9.0
#tool nuget:?package=JetBrains.ReSharper.CommandLineTools&version=2018.3.4
#tool nuget:?package=vswhere&version=2.6.7
	
#addin nuget:https://api.nuget.org/v3/index.json?package=Cake.Npx&version=1.3.0
#addin nuget:https://api.nuget.org/v3/index.json?package=Cake.Issues&version=0.6.2
#addin nuget:https://api.nuget.org/v3/index.json?package=Cake.Issues.InspectCode&version=0.6.1
#addin nuget:https://api.nuget.org/v3/index.json?package=Cake.FileHelpers&version=3.1.0
//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////
var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");
var nugetApiKey = Argument<string>("nugetApiKey", null);

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////
// Define directories.
var projects = GetFiles("./**/*.csproj");
var artifactsDir = Directory("./Artifacts");
string nupkgDir;
var solutionFile = GetFiles("./**/*.sln").First();
var projectName = solutionFile.GetFilenameWithoutExtension().ToString();
var assemblyInfoSubPath = "Properties/AssemblyInfo.cs";
var nugetSource = "https://api.nuget.org/v3/index.json";

// get latest MSBuild version
var vsLatest  = VSWhereLatest();	
var msBuildPathX64 = (vsLatest == null) ? null : vsLatest.CombineWithFilePath("./MSBuild/Current/Bin/MSBuild.exe");

// Error rules for resharper
// Example: {"InconsistentNaming", "RedundantUsingDirective"};
string[] resharperErrorRules = {};
// Paths to exclude from dupFinder
List<string> dupFinderExcludePatterns = projects.Select( x => $"{x.GetDirectory().ToString()}/{assemblyInfoSubPath}").ToList();
string[] dupFinderExcludeCodeRegionsByNameSubstring = { "DupFinder Exclusion" };

Action <NpxSettings> requiredSemanticVersionPackages = settings =>
	settings.AddPackage("semantic-release")
	.AddPackage("@semantic-release/commit-analyzer")
	.AddPackage("@semantic-release/release-notes-generator")
	.AddPackage("@semantic-release/gitlab")
	.AddPackage("@semantic-release/git")
	.AddPackage("@semantic-release/exec")
	.AddPackage("conventional-changelog-eslint");

///////////////////////////////////////////////////////////////////////////////
// SETUP / TEARDOWN
///////////////////////////////////////////////////////////////////////////////
Setup(context =>{
	nupkgDir = $"{artifactsDir.ToString()}/nupkg";
	Information("Running tasks...");
});

Teardown(context =>{
	Information("Finished running tasks.");
});

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////
Task("Clean")
.Description("Cleans all build and artifacts directories")
.Does(() =>{
	var settings = new DeleteDirectorySettings {
		Recursive = true,
		Force = true
	};
	
	var directoriesToDelete = new List<DirectoryPath>();
	
	foreach(var project in projects) {
		directoriesToDelete.Add(Directory($"{project.GetDirectory()}/obj"));
		directoriesToDelete.Add(Directory($"{project.GetDirectory()}/bin"));
	}
	
	directoriesToDelete.Add(artifactsDir);

	foreach(var dir in directoriesToDelete) {
		if (DirectoryExists(dir)) {
			Information($"Cleaning path {dir} ...");
			DeleteDirectory(dir, settings);
		}
	}
});

Task("Restore")
.Does(() =>{
	// Restore all NuGet packages.
	Information($"Restoring {solutionFile}...");
	NuGetRestore(solutionFile, new NuGetRestoreSettings {
		NoCache = true
	});
});

Task("DupFinder")
.Description("Find duplicates in the code")
.Does(() =>{
	var settings = new DupFinderSettings() {
		ShowStats = true,
		ShowText = true,
		OutputFile = $"{artifactsDir}/dupfinder.xml",
		ExcludeCodeRegionsByNameSubstring = dupFinderExcludeCodeRegionsByNameSubstring,
		ExcludePattern = dupFinderExcludePatterns.ToArray(),
		ThrowExceptionOnFindingDuplicates = true
	};
	DupFinder(solutionFile, settings);
});

Task("InspectCode")
.Description("Inspect the code using Resharper's rule set")
.Does(() =>{
	var settings = new InspectCodeSettings() {
		SolutionWideAnalysis = true,
		OutputFile = $"{artifactsDir}/inspectcode.xml",
		ThrowExceptionOnFindingViolations = false
	};
	InspectCode(solutionFile, settings);

	var issues = ReadIssues(
	InspectCodeIssuesFromFilePath($"{artifactsDir}/inspectcode.xml"), Context.Environment.WorkingDirectory);

	Information("{0} issues are found.", issues.Count());

	var errorIssues = issues.Where(issue =>resharperErrorRules.Any(issue.Rule.Contains)).ToList();

	if (errorIssues.Any()) {
		var errorMessage = errorIssues.Aggregate(new StringBuilder(), (stringBuilder, issue) =>stringBuilder.AppendFormat("FileName: {0} Line: {1} Message: {2}{3}", issue.AffectedFileRelativePath, issue.Line, issue.Message, Environment.NewLine));
		throw new CakeException($"{errorIssues.Count} errors detected: {Environment.NewLine}{errorMessage}.");
	}
});

Task("Test")
.Does(() =>{
	NUnit3($"./src/**/bin/{configuration}/*.Tests.dll", new NUnit3Settings {
		// generate the xml file
		NoResults = false,
		Results = new NUnit3Result[] {
			new NUnit3Result() {
				FileName = $"{artifactsDir}/TestResults.xml",
				Transform = Context.Environment.WorkingDirectory + "/nunit3-junit.xslt"
			}
		}
	});
});

Task("NugetPush")
.Does(() =>{
	var nupkgs = GetFiles($"{nupkgDir}/*.nupkg");

	Information("Need to push {0} packages", nupkgs.Count);
	foreach(var nupkg in nupkgs) {
		Information("Pushing {0}", nupkg);
		NuGetPush(nupkg, new NuGetPushSettings {
			Source = nugetSource,
			ApiKey = nugetApiKey
 		});
	}
});

Task("NugetPack")
.Does(() =>{
	foreach(var project in projects) {
			var nuspec = $"{project.GetDirectory()}/{project.GetFilenameWithoutExtension()}.nuspec";
			if(!project.ToString().EndsWith(".Tests") 
			&& FileExists(nuspec))
			{
				Information("Packing {0}...", nuspec);
				
				

				if(!DirectoryExists(nupkgDir)) {
					CreateDirectory(nupkgDir);
				}
				 
				 NuGetPack(project.ToString() ,new NuGetPackSettings 
				 {
					OutputDirectory = nupkgDir,
					Properties = new Dictionary<string, string> { {"Configuration", "Release"}}
				 });
			}
		}
});


Task("UpdateAssemblyInfo")
.Does(() =>{
	Information("Running semantic-release in dry run mode to extract next semantic version number");

	string[] semanticReleaseOutput;
	Npx("semantic-release", "--dry-run", requiredSemanticVersionPackages, out semanticReleaseOutput);

	Information(string.Join(Environment.NewLine, semanticReleaseOutput));

	var nextSemanticVersionNumber = ExtractNextSemanticVersionNumber(semanticReleaseOutput);

	if (nextSemanticVersionNumber == null) {
		Warning("There are no relevant changes. AssemblyInfo won't be updated!");
	} else {
		Information("Next semantic version number is {0}", nextSemanticVersionNumber);

		var assemblyVersion = $"{nextSemanticVersionNumber}.0";

		foreach(var project in projects) {
			CreateAssemblyInfo($"{project.GetDirectory()}/{assemblyInfoSubPath}", new AssemblyInfoSettings {
				Product = project.GetFilenameWithoutExtension().ToString(),
				Title = project.GetFilenameWithoutExtension().ToString(),
				Company = "IT Center, RWTH Aachen University",
				Version = assemblyVersion,
				FileVersion = assemblyVersion,
				InformationalVersion = assemblyVersion,
				Copyright = $"{DateTime.Now.Year} IT Center, RWTH Aachen University",
				Description = $"{project.GetFilenameWithoutExtension().ToString()} is a part of the CoScInE group."
			});
		}
	}
});

Task("Build")
.IsDependentOn("Clean")
.IsDependentOn("Restore")
.Does(() =>{
	if (IsRunningOnWindows()) {
		var frameworkSettingsWindows = new MSBuildSettings {
			Configuration = configuration,
			Restore = true
		};
		
		frameworkSettingsWindows.ToolPath = msBuildPathX64;
		frameworkSettingsWindows.WorkingDirectory = Context.Environment.WorkingDirectory;

		if (configuration.Equals("Release")) {
			frameworkSettingsWindows.WithProperty("DebugSymbols", "false");
			frameworkSettingsWindows.WithProperty("DebugType", "None");
		}
		// Use MSBuild
		Information($"Building {solutionFile}");
		MSBuild(solutionFile, frameworkSettingsWindows);
	}
	else {
		var frameworkSettingsUnix = new XBuildSettings {
			Configuration = configuration
		};

		if (configuration.Equals("Release")) {
			frameworkSettingsUnix.WithProperty("DebugSymbols", "false");
			frameworkSettingsUnix.WithProperty("DebugType", "None");
		}
		// Use XBuild
		Information($"Building {solutionFile}");
		XBuild(solutionFile, frameworkSettingsUnix);
	}
	
	if (configuration.Equals("Release")) {
		if(!DirectoryExists(artifactsDir)) {
			CreateDirectory(artifactsDir);
		}
		
		foreach(var project in projects) {
			if(!project.GetDirectory().ToString().EndsWith(".Tests")
			&& !FileExists($"{project.GetDirectory()}/{project.GetFilenameWithoutExtension()}.nuspec"))
			{
				Information($"Copying {project.GetDirectory()}/bin/{configuration}/* to {artifactsDir}");
				CopyDirectory($"{project.GetDirectory()}/bin/{configuration}/", artifactsDir);
			}
		}
	}

});

Task("SemanticRelease")
.Does(() =>{
	Npx("semantic-release", requiredSemanticVersionPackages);
});

Task("Linter")
.Description("Run DupFinder and InspectCode")
.IsDependentOn("DupFinder")
.IsDependentOn("InspectCode");

Task("LinterAndTest")
.Description("Run Linter and Tests")
.IsDependentOn("Linter")
.IsDependentOn("Test");

Task("Default")
.IsDependentOn("Clean")
.IsDependentOn("Restore")
.IsDependentOn("DupFinder")
.IsDependentOn("InspectCode")
.IsDependentOn("Build")
.IsDependentOn("Test");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////
RunTarget(target);

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
string ExtractNextSemanticVersionNumber(string[] semanticReleaseOutput) {
	var extractRegEx = new System.Text.RegularExpressions.Regex("^.+next release version is (?<SemanticVersionNumber>.*)$");

	return semanticReleaseOutput.Select(line =>extractRegEx.Match(line).Groups["SemanticVersionNumber"].Value).Where(line =>!string.IsNullOrWhiteSpace(line)).SingleOrDefault();
}
