## Virtuoso Setup

Setup for virtuoso database.

Currently, the following steps have to be taken manually:

* For Virtuoso do the following on localhost:8890 or virtuoso.internal.localhost:
     * System Admin →  Packages →  Framework installieren
     * System Admin →  User Account →  SPARQL → click edit → under Account Roles → add SPARQL Update → save
     * ./bin/isql ausführen: GRANT execute ON DB.DBA.L_O_LOOK TO "SPARQL";