﻿using System;
using VDS.RDF;
using VDS.RDF.Query;
using VDS.RDF.Update;

namespace Coscine.VirtuosoSetup
{
    public class SPARQL
    {
        public static readonly string DEFAULT_CONTEXT = "https://purl.org/rwth/";

        private static SparqlRemoteUpdateEndpoint UpdateEndpoint { get; set; }
        private static SparqlRemoteEndpoint QueryEndpoint { get; set; }

        static SPARQL()
        {
            var SPARQLUpdateEndpoint = "http://localhost:8890/sparql";
            var SPARQLQueryEndpoint = "http://localhost:8890/sparql";
            ChangeEndpoints(SPARQLUpdateEndpoint, SPARQLQueryEndpoint);
        }

        public static void ChangeEndpoints(string updateEndpoint, string queryEndpoint)
        {
            UpdateEndpoint = new SparqlRemoteUpdateEndpoint(new Uri(string.Format(updateEndpoint)));
            QueryEndpoint = new SparqlRemoteEndpoint(new Uri(string.Format(queryEndpoint)));
        }

        private static void ProofConnection()
        {
            try
            {
                QueryEndpoint.QueryWithResultSet("");
            }
            catch (RdfException e)
            {
                throw e;
            }
        }

        public static SparqlResultSet Query(string query)
        {
            ProofConnection();
            return QueryEndpoint.QueryWithResultSet(query);
        }
    }
}
